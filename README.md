# OverBot SE, the all-in-one bot for Discord.
[Invite OverBot SE](https://discordapp.com/oauth2/authorize?client_id=649431468254429224&scope=bot&permissions=2146958847) | [OverBot Website](https://overbot.gitlab.io)

OverBot SE is a simple Discord bot that allows you to moderate your server easily, built in with an easy setup command, you can get the bot up and running in as little as one second.

**Common Issues**
* It won't send a message in general chat! - Make sure your general channel is literally named ``general`` or else it won't send welcome messages.
* The bot won't log events! - This is probably because you didn't setup the bot or you renamed the chat log. If you haven't run ``-setup`` to create the chat log channel, also ensure that your chat log channel is named ``chat-log``.

**Commands**

[Required argument], {Optional argument}
* -Info = Shows bot information
* -Setup = Optimizes the bot for the server
* -Avatar {user/yourself} = Shows someone's avatar
* -Kick [User, Reason] = Kick's a member from the guild
* -Ban [User, Reason] = Ban's a member from the guild
* -Warn [User, Reason] = Warn's a member in the guild
* -Report [User, Reason] = Report's a member in the guild.
* -F {reason} = Press f to pay respects!
* -Flip-a-Coin = Heads or Tails?
* -Say [text] = Wait what? I didn't say that!
* -Tableflip = I am so mad!
* -Unflip = Okay, I feel better.
* -Shrug = Don't know, don't care!

**Credits**
* Contact OverThrow - OverThrowDev#3338 (Discord)
* OverBot SE Twitter - https://twitter.com/obotsedev
* Host - Glitch
* Library - discord.js

**Special Thanks**
* [AdminRAT](https://adminrat.gitlab.io) - For the enormous amount of help OverThrow has recieved from him!
* RooXChicken - For being OverThrow's test mouse.
