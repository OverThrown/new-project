const Discord = require("discord.js");
const client = new Discord.Client();
const Enmap = require("enmap");
const fs = require("fs");

let bot = client;

const http = require("http");
const express = require("express");
const app = express();
app.get("/", (request, response) => {
  console.log(Date.now() + " Ping Received");
  response.sendStatus(200);
});
app.listen(process.env.PORT);
setInterval(() => {
  http.get(`http://${process.env.PROJECT_DOMAIN}.glitch.me/`);
}, 280000);


bot.on("ready", () => {
  console.log("OverBot SE is now online.");
  bot.user.setActivity("-commands | Alpha 0.0.4", { type: "PLAYING" });
});

bot.on("guildMemberAdd", member => {
  const embed = new Discord.RichEmbed()
    .setTitle("New Member")
    .setThumbnail(member.user.displayAvatarURL)
    .setDescription(`<@${member.user.id}> just joined the server!`)
    .setColor(0x00ab66)
    .setTimestamp()
    .setFooter(
      "Created by OverThrow, OverBot SE",
        "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048"
    );
  client.channels
    .get(member.guild.channels.find(c => c.name === "general").id)
    .send(embed);
});

bot.on("guildMemberRemove", member => {
  const embed = new Discord.RichEmbed()
    .setTitle("Member Left")
    .setThumbnail(member.user.displayAvatarURL)
    .setDescription(`${member.user.tag} just left the server`)
    .setColor(0xff0000)
    .setTimestamp()
    .setFooter("Created by OverThrow, OverBot SE", "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048"
    );
  client.channels
    .get(member.guild.channels.find(c => c.name === "general").id)
    .send(embed);
});

bot.on("messageDelete", async message => {
  let logs = await message.guild.fetchAuditLogs({type: 72});
  let entry = logs.entries.first();
  
  let delEntry = new Discord.RichEmbed()
    .setTitle("Message Deleted")
    .setColor(0xff0000)
    .addField("Author", message.author.tag, true)
    .addField("Channel", message.channel, true)
    .addField("Message", message.content)
    .setTimestamp()
    .setFooter(`Created by OverThrow, OverBot SE | Message ID: ${message.id}`,  "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048")
  let channel = message.guild.channels.find(c => c.name === "chat-log");
  channel.send(delEntry);
  
  bot.on("channelCreate", async message => { 
    let logs = await message.guild.fetchAuditLogs({type: 10});
    let entry = logs.entries.first();
    
    let embed = new Discord.RichEmbed()
      .setTitle("Channel Created")
      .setColor(0x00ab66)
      .addField("ID", channel.id, true)
      .setTimestamp()
      .setFooter("Created by OverThrow, OverBot SE", "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048")
    bot.channels
      .get(message.guild.channels.find(c => c.name === "chat-log").id)
      .send(embed)
  }) 
})

//------------------------------------commands-----------------------------------------------------

bot.on("message", async message => {
  const ownerid = process.env.OWNERID;
  const prefix = process.env.PREFIX;

 const args = message.content.slice(prefix.length).trim().split(/ +/g);
 const command = args.shift().toLowerCase();

  if (command === `setup`) {
    if (!message.member.hasPermission('ADMINISTRATOR')) return message.channel.send(":x: Only a member with the permission `\`\ADMINISTRATOR`\`\ can run this command!");
    const embed = new Discord.RichEmbed()
      .setTitle("Setup")
      .setDescription("Setting up the server for OverBot...")
      .setColor(0x00ab66)
      .setTimestamp()
      .setFooter(
        "Created by OverThrow, OverBot SE",
        "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048"
      );
    message.channel.send(embed);
    message.guild.createRole({ name: "OverBot-Admin" });
    message.guild.createChannel("chat-log", { type: "text" }).then(message.channel.send(":white_check_mark: The server has successfully been set up for OverBot SE!"));
  }

  //----------
  if (message.content === `-commands`) {
    const embed = new Discord.RichEmbed()
      .setTitle("Commands")
      .setThumbnail(
        "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048"
      )
      .setColor(0xfef65b)
      .setDescription("Here are the commands you can use!\n{required}, (optional)")
      .addField("-Info", "Shows bot information and additional credits.")
      .addField("-Setup", "Run's the server set up.")
      .addField("-Avatar {user || yourself}", "Show's the avatar of a member in the guild.")
      .addField("-Kick {user} {reason}", "Kick\'s a member from the guild.")
      .addField("-Ban {user} {reason}", 'Ban\'s a member from the guild.')
      .addField("-Warn {user} {reason}", "Warn's a member in the guild.")
      .addField("-Report {user} {report reason}", "Reports a member in the guild.")
      .addField("-F (to whom)", "Press f to pay respects...")
      .addField("-Flip-A-Coin", "Flips a coin, heads or tails?")
      .addField("-Say {text}", "Says whatever you want it to say.")
      .addField("-Purge {number}", "Bulk deletes a number of messages.")
      .addField("-Tableflip", "I'm really madddd!")
      .addField("-Unflip", "Ok, I'm cooling down.")
      .addField("-Shrug", "Don't know don't care!")
      .setTimestamp()
      .setFooter(
        "Created by OverThrow, OverBot SE",
        "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048"
      );
    message.channel.send(embed);
  }
  //----------
  if (message.content.startsWith === `-info`) {
    const embed = new Discord.RichEmbed()
      .setTitle("Information")
      .setDescription(
        "Hello! I am OverBot SE, the second version of OverBot. Disclaimer: I am in really early development, some features are prone to changing and bugs will arise. Please report any you find to OverThrow! New commands are always in the working so be sure to keep your eyes peeled!"
      )
      .setThumbnail(
        "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048"
      )
      .setColor(0x00ab66)
      .addField("Contact OverThrow", "OverThrowDev#3338")
      .addField("Join the support server:", "https://discord.gg/HUn9J3X")
      .addField("OverBot SE Twitter:", "https://twitter.com/obotsedev")
      .addField("Host:", "Glitch")
      .addField(
        "Special thanks to:",
        "AdminRAT - for the enormous amount of help I've recieved from him!\nRooXChicken - for being my test mouse."
      )
      .setTimestamp()
      .setFooter(
        "Created by OverThrow, OverBot SE",
        "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048"
      );
    message.channel.send(embed);
  }
  //----------
  if (message.content === `-avatar`) {
    const user = message.mentions.users.first() || message.author;
    const avatarEmbed = new Discord.RichEmbed()
      .setColor(0x00ab66)
      .setAuthor(user.username, user.displayAvatarURL)
      .setImage(user.displayAvatarURL)
      .setTimestamp()
      .setFooter(
        "Created by OverThrow, OverBot SE",
        "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048"
      );
    message.channel.send(avatarEmbed);
  }

  if (message.content.startsWith("-kick")) {
    let reason = args.slice(1).join(" ");
    if (!reason) reason = "No reason provided";
    const user = message.mentions.users.first();
    if (!message.member.hasPermission('KICK_MEMBERS')) return message.channel.send(":x: You do not have permission to use that command!");


    if (user) {
      const member = message.guild.member(user);

      if (member) {
        member
          .kick(reason)
          .then(() => {
            message.reply(
              `:white_check_mark: Successfully kicked ${member.user.tag} for ${reason}!`
            );
            const embed = new Discord.RichEmbed()
              .setTitle("Member Kicked")
              .setThumbnail(user.displayAvatarURL)
              .addField("Member:", member.user.tag)
              .addField("ID:", user.id)
              .addField("Reason:", reason)
              .addField("Kicked by:", message.author)
              .setColor(0xff0000)
              .setTimestamp()
              .setFooter(
                "Created by OverThrow, OverBot SE",
                "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048"
              );
            client.channels
              .get(message.guild.channels.find(c => c.name === "chat-log").id)
              .send(embed);
            member.user.send(
              "You have been kicked from " +
                message.guild.name +
                " for " +
                reason
            );
          })
          .catch(err => {
            message.reply(`:x: I was unable to kick ${user}`);
            console.error(err);
          });
      } else {
        message.reply(":x: That user isn't in this guild!");
      }
    } else {
      message.reply(":x: You didn't mention a user to kick!");
    }
  }

  if (message.content.startsWith("-ban")) {
      let reason = args.slice(1).join(" ");
      if (!reason) reason = "No reason provided";
      const user = message.mentions.users.first();
      if (!message.member.hasPermission('BAN_MEMBERS')) return message.channel.send(":x: You do not have permission to use that command!");

      if (user) {
        const member = message.guild.member(user);

        if (member) {
          member
            .ban({ reason: reason })
            .then(() => {
              message.reply(":white_check_mark: Successfully banned " + user.tag + " for " + reason
              );

              const embed = new Discord.RichEmbed()
                .setTitle("Ban")
                .setColor(0xff0000)
                .setThumbnail(user.displayAvatarURL)
                .addField("Member:", member)
                .addField("ID:", member.id)
                .addField("Reason:", reason)
                .addField("Banned by:", message.author)
                .setTimestamp()
                .setFooter(
                  "Created by OverThrow, OverBot SE",
                  "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048");
                client.channels
                  .get(message.guild.channels.find(c => c.name === "chat-log").id)
                  .send(embed);
            })
            .catch(err => {
              message.reply(":x: I was unable to ban that member!");

              console.error(err);
            });
        } else {
          message.reply(":x: That user isn't a member to this guild!");
        }
      } else {
        message.reply(":x: You didn't mention a member to ban!");
      }
    }
  
  if (message.content.startsWith("-warn")) {
    let reason = args.slice(1).join(" ");
    if (!reason) reason = "No reason provided!"
    const user = message.mentions.users.first();
    let warnMember = message.mentions.members.first();
    let logChannel = message.guild.channels.find(c => c.name === "chat-log");
    if (!message.member.hasPermission("KICK_MEMBERS")) return message.channel.send(":x: You don't have permission to use that command!")
    let reportLog = "report-log";
    if (!reportLog) return message.channel.send("The report log does not exist, please contact a server administrator!")
    if (!warnMember) return message.channel.send(":x: Please mention a member to warn!");
    const wembed = new Discord.RichEmbed()
      .setTitle("Warn")
      .setThumbnail(user.displayAvatarURL)
      .setColor(0xff0000)
      .addField("Member:", warnMember)
      .addField("ID:", warnMember.id)
      .addField("Reason:", reason)
      .addField("Warned by:", message.author)
      .setTimestamp()
      .setFooter("Created by OverThrow, OverBot", "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048");
    client.channels
      .get(message.guild.channels.find(c => c.name === "chat-log").id)
      .send(wembed);
    warnMember.user.send(":x: You have been warned in " + message.guild + " for " + reason + "!");
    message.channel.send(":white_check_mark: Successfully warned " + warnMember + " for " + reason + "!");
   }
  
  if (message.content.startsWith("-report")) {
    let user = message.mentions.users.first();
    let reason = args.slice(1).join(" ");
    const reportLog = message.guild.channels.find(c => c.name === "report-log")
      if (reportLog === null) return message.channel.send(":x: The report-log channel does not exist! Contact an administrator and tell them to run -setup!");
      else {
        if (!user) return message.channel.send(":x: Please mention a member in the guild!")
        if (!reason) return message.channel.send(":x: Please list a reason for your report!")
    
        const rembed = new Discord.RichEmbed()
          .setTitle("Report")
          .setThumbnail(user.displayAvatarURL)
          .setColor(0xff0000)
          .addField("Member Reported", user)
          .addField("Reason", reason)
          .addField("Reported by", message.author)
          .setTimestamp()
          .setFooter("Created by OverThrow, OverBot SE", "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048")
        client.channels
          .get(message.guild.channels.find(c => c.name === "report-log").id)
          .send(rembed);
        message.channel.send(`:white_check_mark: Successfully reported ${user} for ${reason}!`);
      }
  }
  
  if (message.content.startsWith("-f")) {
    const fReason = args.slice().join(" ");
    
    if (!fReason) return message.channel.send(message.author + " " + "has paid their respects.")
    else {
    
    message.channel.send(message.author + " " + "has paid their respects for" + " " + fReason);
    }
  }
  
  if (message.content === "-flip-a-coin") {
    const number = Math.floor(Math.random() * 2);
    
  if (number === 0) return message.channel.send("Heads")
  if (number === 1) return message.channel.send("Tails");
  }
  
  if (message.content === "-say") {
    const sayText = args.slice(1).join(" ");
    
    message.channel.send(sayText)
    message.delete().catch(O_o=>{});
    }
  
    if (message.content === "-tableflip") {
    message.channel.send(" (╯°□°）╯︵ ┻━┻");
    message.delete().catch(O_o=>{});
    }
  
  if (message.content === "-unflip") {
    message.channel.send("┬─┬ ノ( ゜-゜ノ)")
    message.delete().catch(O_o=>{});
  }
  
  if (message.content === "-shrug") {
    message.channel.send("¯\_(ツ)_/¯")
    message.delete().catch(O_o=>{});
  }
  
  if (message.content.startsWith("-mute")) {
    let reason = args.slice(1).join(" ");
    let member = message.mentions.members.first();
    let channel = message.guild.channels.find(c => c.name === "chat-log");
    let role = message.guild.roles.find(r => r.name === "muted");
    
    if (channel === null) {
      message.channel.send(":x: The chat-log does not exist, please contact an administrator!");
    } else {
      if (!role) return message.channel.send(":x: The muted role does not exist, please contact an administrator!");
      if (!member) return message.channel.send(":x: You need to mention a member to mute.");
      if (!reason) reason = "No reason provided!";
      if (!message.member.hasPermission("MANAGE_ROLES")) return message.channel.send(":x: You do not have permission to run that command!")
      
      member.addRole(role).catch(err => {
        message.channel.send(`I couldn't mute ${member} because of ${err}`);
        
        console.log(err);
      })
      message.channel.send(":white_check_mark: Successfully muted " + member + " for " + reason)
      
      const embed = new Discord.RichEmbed()
        .setTitle("Member Muted")
        .setColor(0xff0000)
        .addField("Member", member)
        .addField("Reason", reason)
        .setTimestamp()
        .setFooter("Created by OverThrow, OverBot SE", "https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048")
      
      channel.send(embed);
    }
  }
  
  if (message.content.startsWith("-purge")) {
  const modLogChannel = message.guild.channels.find(c => c.name === "chat-log");
  const deleteCount = parseInt(args[0], 10);
    
  if(!message.member.hasPermission("MANAGE_MESSAGES")) return message.channel.send(":x: You do not have permission to run that command!")

  if(!deleteCount || deleteCount < 2 || deleteCount > 100)
    return message.reply("Please provide a number between 2 and 100 for the number of messages to delete");

  
  const fetched = await message.channel.fetchMessages({limit: deleteCount});
  message.channel.bulkDelete(fetched)
    .catch(error => message.reply(`Couldn't delete messages because of: ${error}`));
  
  const embed = new Discord.RichEmbed()
    .setTitle("Purge")
    .addField("Message Count", deleteCount)
    .addField("Purged by", message.author)
    .setColor(0x426cf5)
    .setTimestamp()
    .setFooter('Created by OverThrow, OverBot SE', 'https://cdn.discordapp.com/avatars/649431468254429224/0b63291c1e7342c2320ca5c3cce806ca.png?size=2048');
  client.channels
    .get(message.guild.channels.find(c => c.name === "chat-log").id)
    .send(embed).then(message.delete().catch(O_o=>{}));
  }
});
//--------------------------------

client.login(process.env.TOKEN);
